# Inicio

iniciando a conocer ELM

para iniciar a usar este proyecto usa:

```
elm install
```

y para compilar usando linux

```
sh elm.sh
```

o usando windows:

```
elm make vcu/Inicio.elm --output vcu/Inicio.elm.js
```