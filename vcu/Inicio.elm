port module Inicio exposing 
    ( main
    )

-- IMPORTS ###################################################################

import Browser
import Html exposing (Html, text, div, button)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)

-- TYPES ###################################################################

type alias Model =
    { seccion : Seccion
    , botones : List String
    , listaColores : List String
    }

type alias Flags =
    { botones : List String
    , seccionArranque : String
    }

type alias ParamsAplicarColores =
    { color : String
    , target : String
    }

type Msg
    = NoOp
    | CambiarSeccion Seccion
    | UObtenerColores
    | UGuardarColores (List String)
    | UAplicarColorFondo String

type Seccion
    = Inicio
    | Contacto
    | Galeria
    | Colores
    | Error
    | Musica Int GeneroMusical

type GeneroMusical
    = Rock
    | Pop

-- CONSTANTS ###################################################################

-- INITIAL MODELS ###################################################################

initialModel : Flags -> ( Model, Cmd Msg ) 
initialModel flags =
    let
        newSeccion = str2seccion flags.seccionArranque
    in
        ( { seccion = newSeccion
            , botones = flags.botones
            , listaColores = []
            }
        , Cmd.none
        )

-- UPDATE ###################################################################

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )
        
        CambiarSeccion newSeccion ->
            uCambiarSeccion newSeccion model
        
        UObtenerColores ->
            uObtenerColores model
        
        UGuardarColores colores ->
            uGuardarColores colores model
        
        UAplicarColorFondo color ->
            uAplicarColorFondo color model

-- UPDATES ###################################################################

uAplicarColorFondo : String -> Model -> ( Model, Cmd Msg )
uAplicarColorFondo color model =
    ( model
    , portOutAplicarColorFondo
        { color = color
        , target = "button"
        }
    )

uGuardarColores : List String -> Model -> ( Model, Cmd Msg )
uGuardarColores colores model =
    ( { model | listaColores = colores }
    , Cmd.none
    )

uObtenerColores : Model -> ( Model, Cmd Msg )
uObtenerColores model =
    ( model
    , portOutGetColors ()
    )

uCambiarSeccion : Seccion -> Model -> ( Model, Cmd Msg )
uCambiarSeccion newSeccion model =
    ( { model | seccion = newSeccion }
    , Cmd.none
    )

-- CALLBACKS ###################################################################

-- JSON DECODERS ###################################################################

-- METHODS ###################################################################

extraerValores : List Seccion -> String
extraerValores lista =
    case lista of
        [] ->
            "es vacía"
    
        e :: [] -> -- [Inicio] == Inicio :: []
            "sólo tiene un elemento y es:" ++ (seccion2str e)

        e :: e2 :: [] -> -- [Inicio, Galeria] == Inicio :: Galeria :: []
            "tiene 2 elementos y son:" ++ (seccion2str e) ++ (seccion2str e2)

        _ -> -- [Inicio, Galeria, ...] == Inicio :: Galeria :: []
            "tiene 3 o más elementos"

extraerDeMaybe : Maybe Seccion -> String
extraerDeMaybe podriaSeccion =
    case podriaSeccion of
        Just x ->
            "el elemento es:" ++ (seccion2str x)

        Nothing ->
            "No tiene valor"

extraerDeTuplas : (Seccion, Seccion) -> String
extraerDeTuplas tupla =
    case tupla of
        (Inicio, Inicio) ->
            "está en su sección favorita:"

        (Galeria, Galeria) ->
            "está en su sección favorita:"

        (Galeria, Contacto) ->
            "la siguiente sección es tu favorita"

        (Contacto, _) ->
            "está en el contacto"

        (actual, _) ->
            "la sección actual es: " ++ (seccion2str actual)

extraerNum : Int -> String
extraerNum num =
    case num of
        1 ->
            "está en su sección 1"

        2 ->
            "está en su sección 2"

        x ->
            "está en su sección" ++ (String.fromInt x)

extraerGenero : Seccion -> Maybe GeneroMusical
extraerGenero seccion =
    case seccion of
        Musica _ gen ->
            Just gen

        _ ->
            Nothing

comparacionX : Int -> Int -> String
comparacionX num1 num2 =
    case (num1 == num2, num1 > num2) of
        (False, True) ->
            "El num1 > el num2"

        (True, _) ->
            "los nums son iguales"

        (False, False) ->
            "El num1 < el num2"

extraerDeTuplas2 : (Seccion, Seccion) -> String
extraerDeTuplas2 (a, b) =
    "la tupla es:" ++ (seccion2str a) ++ (seccion2str b)

-- convierte string a seccion
str2seccion : String -> Seccion
str2seccion seccionIN =
    case seccionIN of
        "Inicio" -> Inicio

        "Contacto" -> Contacto

        "Galeria" -> Galeria

        _ -> Error

-- convierte seccion a string
seccion2str : Seccion -> String
seccion2str sec =
    case sec of
        Inicio ->
            "Inicio"
    
        Contacto ->
            "Contacto"

        Galeria ->
            "Galería"

        Musica _ Pop ->
            "Musica Pop"

        Musica _ Rock ->
            "Musica Rock"

        Colores -> "Colores"

        Error -> "Error"

-- VIEWS ###################################################################

-- va a mostrar las opciones de colores
p_viewColores : Model -> Html Msg
p_viewColores model =
    let
        htmlListaBotones =
            model.listaColores
            |> List.map (
                \color ->
                    button [ style "background-color" color
                        , onClick <| UAplicarColorFondo color
                        ]
                        [ text color ]
            )
    in
        div []
            [ button [ onClick UObtenerColores ] [ text "Obtener colores" ]
            , div [] htmlListaBotones
            ]

p_viewContenidoSeccion : Model -> Html Msg
p_viewContenidoSeccion model =
    case model.seccion of
        Colores ->
            p_viewColores model
    
        _ ->
            text ""

view : Model -> Html Msg
view model =
    let
        strSeccion = seccion2str model.seccion
        listHtmlBotonesFlags =
            model.botones
            |> List.map (
                \textoBoton ->
                    button [] [ text textoBoton ]
            )
    in
        div []
            [ button [ onClick <| CambiarSeccion Inicio ] [ text "Inicio" ]
            , button [ onClick <| CambiarSeccion Contacto ] [ text "Contacto" ]
            , button [ onClick <| CambiarSeccion Galeria ] [ text "Galeria" ]
            , button [ onClick <| CambiarSeccion Colores ] [ text "Colores" ]
            , div []
                [ text <| "Te encuentras en la sección: " ++ strSeccion
                ]
            , div [] listHtmlBotonesFlags
            , p_viewContenidoSeccion model
            ]

-- OUT Ports ###################################################################

port portOutGetColors : () -> Cmd msg
port portOutAplicarColorFondo : ParamsAplicarColores -> Cmd msg

-- IN Ports ###################################################################

port portInColores : (List String -> msg) -> Sub msg

-- SUBSCRIPTIONS ###################################################################

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ portInColores UGuardarColores
        ]

-- END ###################################################################

main : Program Flags Model Msg
main =
    Browser.element
        { init = initialModel
        , view = view
        , update = update
        , subscriptions = subscriptions
        }